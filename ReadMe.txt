Abgabe - Lukas Kuhn - 868966 - Eye-Tracker-Survey - Advanced Interactive Systems

Unity 5.5.4p4

Durchführung (Windows)

Schritt 1 - Schließen Sie den Eye-Tracker an Ihrem PC an und Installieren Sie die nötige Software
Schritt 2 - Starten Sie die Eye-Tracker-Survey.exe im Fenstermodus bei einer Auflösung von 1600 x 1024

Durchführung (Linux / Mac)

Schritt 1 - Öffnen Sie das Projekt mit Unity
Schritt 2 - Öffnen Sie unter Assets/Scenes die Szene "Intro"
Schritt 3 - Führen Sie die Szene im Vollbildmodus aus
Schritt 4 - Folgen Sie den Anweisungen der Anwendung