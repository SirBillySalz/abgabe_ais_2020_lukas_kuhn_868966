using UnityEngine;
using Tobii.Gaming;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(GazeAware))]
public class GazeLooking : MonoBehaviour {

	public Color selectionColor;

	private GazeAware _gazeAwareComponent;
	public SpriteRenderer _sprite;
	public Text _text;
	public Text _continue;

	private float seconds;

	void Start() {
		_gazeAwareComponent = GetComponent<GazeAware>();
		_text.enabled = false;
	}

	void Update() {

		if (!TobiiAPI.GetGazePoint().IsRecent()) {
			_text.enabled = true;
			_continue.enabled = false;
		} else {
			_text.enabled = false;
			_continue.enabled = true;
		}

		if (_gazeAwareComponent.HasGazeFocus) {
			this.seconds += Time.deltaTime;
			if (this.seconds <= 0.7) {
				_sprite.color -= new Color(0,0,0,0.01f);
			} else {
				SceneManager.LoadScene("Umfrage");
			}
			
		} else {
			_sprite.color = new Color(0,0,0,1);
			this.seconds = 0;
		}
	}
}
