using UnityEngine;

public class ContinueDescission : MonoBehaviour {

    public GazeLookingClick choice_one;
    public GazeLookingClick choice_two;
    public SpriteRenderer element;

	void Start() {
		element.enabled = false;
	}

	void Update() {
        if (choice_one.isComplete()) {
            element.enabled = true;
        }
        if (choice_two.isComplete()) {
            element.enabled = true;
        }
	}

}
