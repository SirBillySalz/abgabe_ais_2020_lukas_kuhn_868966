using UnityEngine;
using Tobii.Gaming;
using UnityEngine.UI;

[RequireComponent(typeof(GazeAware))]
public class SafetyButton : MonoBehaviour {

	public ProgressBar progressOne;
	public ProgressBar progressTwo;
	public Text textOne;
	public Text textTwo;

	private GazeAware _gazeAwareComponent;

	private float seconds;

	void Start() {
		_gazeAwareComponent = GetComponent<GazeAware>();
		progressOne.current = 0;
		progressTwo.current = 0;
		textOne.enabled = true;
		textTwo.enabled = false;
	}

	void Update() {

		if (this.seconds > 0.7f && progressOne.current >= (progressOne.maximum / 2)) {
			textOne.enabled = false;
			textTwo.enabled = true;
		}

		if (this.seconds > 1.4f && progressTwo.current >= (progressTwo.maximum / 2)) {
			Application.Quit();
		}

		if (_gazeAwareComponent.HasGazeFocus) {
			this.seconds += Time.deltaTime;
			
			if (progressOne.current <= (progressOne.maximum / 2)) {
				progressOne.current = this.seconds;
			} else if (progressTwo.current <= (progressTwo.maximum / 2) && progressOne.current >= (progressOne.maximum / 2)) {
				progressTwo.current = this.seconds - 0.7f;
			}
			
		} else {
			this.seconds = 0;
			progressOne.current -= 2.0f;
			progressTwo.current -= 2.0f;
			textOne.enabled = true;
			textTwo.enabled = false;
		}
	}

}
