using UnityEngine;
using Tobii.Gaming;
using UnityEngine.UI;

[RequireComponent(typeof(GazeAware))]
[RequireComponent(typeof(ProgressBar))]
public class ResetableButton : MonoBehaviour {

	public Image activeImage;
	public Color selectionColor;

	private GazeAware _gazeAwareComponent;
	public MeshRenderer _meshRenderer;
	private ProgressBar _progressBar;

	private float seconds;
	private Color basicColor;

	void Start() {
		_progressBar = GetComponent<ProgressBar>();
		_gazeAwareComponent = GetComponent<GazeAware>();
		basicColor = activeImage.color;
		_progressBar.current = 0;
	}

	void Update() {

		if (this.seconds > 0.7f && _progressBar.current >= _progressBar.maximum) {
			Reset();
		}

		if (_gazeAwareComponent.HasGazeFocus) {
			this.seconds += Time.deltaTime;
			
			if (_progressBar.current <= _progressBar.maximum) {
				_progressBar.current = this.seconds;
			}
			
		} else if (_progressBar.current >= _progressBar.minimum) {
			this.seconds = 0;
			_progressBar.current -= 2.0f;
		}
	}

	public void Reset() {
		activeImage.color = (activeImage.color == selectionColor) ? basicColor : selectionColor;
		_progressBar.current = 0;
		this.seconds = 0;
	}

	public bool isComplete() {
		return (activeImage.color == selectionColor);
	}

}
