using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalizeDescission : MonoBehaviour {

    public ResetableButton choice_one;
    public ResetableButton choice_two;
    public ResetableButton choice_three;
    public ResetableButton choice_four;
    public ResetableButton choice_five;
    public SpriteRenderer element;

    void Start() {
		element.enabled = false;
	}

	void Update() {
        if (choice_one.isComplete()) {
            element.enabled = true;
        }
        if (choice_two.isComplete()) {
            element.enabled = true;
        }
        if (choice_three.isComplete()) {
            element.enabled = true;
        }
        if (choice_four.isComplete()) {
            element.enabled = true;
        }
        if (choice_five.isComplete()) {
            element.enabled = true;
        }
	}

}
