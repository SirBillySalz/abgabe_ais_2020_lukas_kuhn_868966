﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode()]
public class ProgressBar : MonoBehaviour {

	public float minimum;
	public float maximum;
	public float current;
	public Image mask;
	public Image fill;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		getCurrentFillState();
	}

	void getCurrentFillState() {
		float currentOffset = current - minimum;
		float maximumOffset = maximum - minimum;
		float fillAmount = currentOffset / maximumOffset;
		mask.fillAmount = fillAmount;
	}
}
