using UnityEngine;
using Tobii.Gaming;
using UnityEngine.UI;

[RequireComponent(typeof(GazeAware))]
[RequireComponent(typeof(ProgressBar))]
public class GazeLookingClick : MonoBehaviour {

	public Image activeImage;
	public Color selectionColor;
	public GazeLookingClick otherChoice;

	private GazeAware _gazeAwareComponent;
	public MeshRenderer _meshRenderer;
	private ProgressBar _progressBar;

	private float seconds;
	private Color basicColor;

	void Start() {
		_progressBar = GetComponent<ProgressBar>();
		_gazeAwareComponent = GetComponent<GazeAware>();
		basicColor = activeImage.color;
		_progressBar.current = 0;
	}

	void Update() {

		if (this.seconds > 0.7f && _progressBar.current >= _progressBar.maximum) {
			activeImage.color = selectionColor;
			otherChoice.Reset();
		}

		if (_gazeAwareComponent.HasGazeFocus && !(activeImage.color == selectionColor)) {
			this.seconds += Time.deltaTime;
			
			if (_progressBar.current <= _progressBar.maximum) {
				_progressBar.current = this.seconds;
			}
			
		} else if (_progressBar.current >= _progressBar.minimum) {
			this.seconds = 0;
			_progressBar.current -= 2.0f;
		}
	}

	public void Reset() {
		activeImage.color = basicColor;
		_progressBar.current = 0;
	}

	public bool isComplete() {
		return (activeImage.color == selectionColor);
	}

}
